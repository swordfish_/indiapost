# README #

Sending a courier from India to Sweden takes about 15 to 20 days. Speed Post in India offers a tracking service (that doesn't suck) for International Shipments. This python script scraps through the tracking website, and notifies you if there is an update.

### How do I get set up? ###

* Requirements: Python 2.7
* Dependencies: BeautifulSoup, urllib2 libs for python
* Deployment instructions: Make sure to install mail-utils on your machine so that you can use the mail command to send an e-mail. There are several ways this can be achieved, I probably have a guide in my old github repo, will update this later. Once mail is configured, add your tracking ID, and change e-mail addresses in SPstatuscheck.py. I have a cron job that triggers this bi-hourly. Increasing frequency any further is an overkill, India Post is not that fast. 

### Contribution guidelines ###

* Feel free to clone or submit pull requests