#!/usr/bin/python
from BeautifulSoup import BeautifulSoup
import urllib2
import os
import syslog

page = urllib2.urlopen('http://ipsweb.ptcmysore.gov.in/ipswebtracking/IPSWeb_item_events.asp?itemid=INSERTTRACKERID&Submit=Submit')
soup = BeautifulSoup(page)
# tabl1 and tabl2 are the CSS style classes used by the status display table
status_table = soup.findAll("tr", { "class" : ["tabl1", "tabl2"] })
length = len(status_table)
# keep track of the status count history
if (os.path.isfile("checkpoints.txt")):
    fileobject = open("checkpoints.txt", "r+")
else:
    # If status tracker file doesn't exist, create a file, update with current status count, and exit
    # ToDo: refactor this to send the latest status update before exit 
    fileobject = open("checkpoints.txt", "w+")
    fileobject.write(str(length))
    fileobject.close()
    exit(0)

if length > int(fileobject.read()):
    # always get the latest status update 
    table_soup = BeautifulSoup(str(status_table[length - 1]))
    data = table_soup.findAll("td")
    message = data[0].getText() + "\n" + data[1].getText() + "\n" + data[2].getText() + "\n"
    message += data[3].getText() + "\n" + data[4].getText() + "\n" + data[5].getText()
    cmdstring = 'echo \"%s\" | mail -r no-reply@yourserver.com -s \"Re: Speed Post\" john@doe.com' %(message)    
    if (os.system(cmdstring) == 0):
        # do not append
        fileobject.seek(0,0)
        fileobject.write(str(length))    
        syslog.syslog(syslog.LOG_DEBUG, 'SPstatuscheck.py: E-mail sent')
    else:
        syslog.syslog(syslog.LOG_ALERT, 'SPstatuscheck.py: Call to mail failed')
else:
    syslog.syslog(syslog.LOG_DEBUG, 'SPstatuscheck.py: Nothing to do')
fileobject.close()
